module gitote.in/dimensi0n/persona

require (
	github.com/jinzhu/gorm v1.9.1
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
)
